#!/bin/bash

## 00.db-conf.sh

PERMISSION_INI=/var/www/html/app/config/permission.ini
APP_INI=/var/www/html/app/config/app.ini
LOGS_INI=/var/www/html/app/config/log.ini
COMMUNICATION_INI=/var/www/html/app/config/communication.ini

## Cleanup Files
echo "" > $PERMISSION_INI
echo "" > $APP_INI
echo "" > $LOGS_INI
echo "" > $COMMUNICATION_INI

## Config 
echo "host = ${DB_HOST}" >> $PERMISSION_INI
echo "host = ${DB_HOST}" >> $APP_INI
echo "host = ${DB_HOST}" >> $LOGS_INI
echo "host = ${DB_HOST}" >> $COMMUNICATION_INI

echo "port = ${DB_PORT}" >> $PERMISSION_INI
echo "port = ${DB_PORT}" >> $APP_INI
echo "port = ${DB_PORT}" >> $LOGS_INI
echo "port = ${DB_PORT}" >> $COMMUNICATION_INI

echo "user = ${DB_USER}" >> $PERMISSION_INI
echo "user = ${DB_USER}" >> $APP_INI
echo "user = ${DB_USER}" >> $LOGS_INI
echo "user = ${DB_USER}" >> $COMMUNICATION_INI

echo "pass = ${DB_PASS}" >> $PERMISSION_INI
echo "pass = ${DB_PASS}" >> $APP_INI
echo "pass = ${DB_PASS}" >> $LOGS_INI
echo "pass = ${DB_PASS}" >> $COMMUNICATION_INI

echo "name = ${DB_NAME}" >> $PERMISSION_INI
echo "name = ${DB_NAME}" >> $APP_INI
echo "name = ${DB_NAME}" >> $LOGS_INI
echo "name = ${DB_NAME}" >> $COMMUNICATION_INI

echo "type = ${DB_TYPE}" >> $PERMISSION_INI
echo "type = ${DB_TYPE}" >> $APP_INI
echo "type = ${DB_TYPE}" >> $LOGS_INI
echo "type = ${DB_TYPE}" >> $COMMUNICATION_INI
